# TP1 : Mise en jambe

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

### . En invite de commande

- **ipconfig -all**

```
Carte réseau sans fil Wi-Fi :

    Suffixe DNS propre à la connexion. . . : auvence.co
    Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
>>  Adresse physique . . . . . . . . . . . : 08-71-90-87-B9-8C
    DHCP activé. . . . . . . . . . . . . . : Oui
    Configuration automatique activée. . . : Oui
    Adresse IPv6 de liaison locale. . . . .: fe80::7967:acde:dfb4:2037%18(préféré)
>>  Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.196(préféré)
    Masque de sous-réseau. . . . . . . . . : 255.255.252.0
    [...]
```
```
>>  Passerelle par défaut. . . . . . . . . : 10.33.3.253
```
```
Carte Ethernet Ethernet :

>> Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
>> Adresse physique . . . . . . . . . . . : 00-D8-61-87-1C-74
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
```
### . En graphique (GUI : Graphical User Interface)
![](https://i.imgur.com/gWnKY1V.png)

### Question : à quoi sert la gateway dans le réseau d'YNOV ?
Dans notre cas la gateway nous permet de se connecter au réseau "Internet"

### 2. Modifications des informations
### A. Modification d'adresse IP (part 1)
- **Avant**

![](https://i.imgur.com/GJiP7AY.png)

- **Après**

![](https://i.imgur.com/zULKJ4g.png)

### Question: **Expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération**.

Selon le réseau, il est possible que le serveur se trompe ou ne détecte pas l'adresse IP de la machine. Ceci est dû au fait que ce n'est pas le serveur qui a attribué l'IP en question.

### B. Table ARP

Affichage de la table ARP: ***arp -a***

```
Interface : 10.33.2.196 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.33.0.1             3c-58-c2-9d-98-38     dynamique
  10.33.0.100           e8-6f-38-6a-b6-ef     dynamique
  10.33.1.90            d0-ab-d5-18-55-f6     dynamique
  10.33.1.238           50-76-af-88-6c-0b     dynamique
  10.33.1.248           e8-84-a5-24-94-c9     dynamique
  10.33.2.48            80-91-33-9c-bf-0d     dynamique
  10.33.2.56            c0-3c-59-a9-e0-75     dynamique
  10.33.3.120           3c-6a-a7-c3-0a-28     dynamique
  10.33.3.123           38-fc-98-e5-ae-61     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

Pour identifier la MAC de ma carte réseau j'utilise l'IP de la Gateway
Commande: ***ipconfig***

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::7967:acde:dfb4:2037%18
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.196
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
>> Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

Donc la MAC est `00-12-00-40-4c-bf`

```
Interface : 10.33.2.196 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.33.3.253           00-12-00-40-4c-bf     dynamique
```

Après avoir fait des pings sur des ip random on obtient une nouvelle table ARP
Commande: ***arp -a***

```
Interface : 10.33.2.196 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.33.0.1             3c-58-c2-9d-98-38     dynamique
  10.33.0.3             40-ec-99-8d-22-23     dynamique
  10.33.0.100           e8-6f-38-6a-b6-ef     dynamique
  10.33.1.41            9c-fc-e8-36-7b-ee     dynamique
  10.33.1.90            d0-ab-d5-18-55-f6     dynamique
  10.33.1.238           50-76-af-88-6c-0b     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.1.248           e8-84-a5-24-94-c9     dynamique
  10.33.2.48            80-91-33-9c-bf-0d     dynamique
  10.33.2.56            c0-3c-59-a9-e0-75     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
  10.33.3.120           3c-6a-a7-c3-0a-28     dynamique
  10.33.3.123           38-fc-98-e5-ae-61     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

Dont les nouvelles MAC des adresses IP ping

```
Interface : 10.33.2.196 --- 0x12
  Adresse Internet      Adresse physique      Type
  10.33.0.3             40-ec-99-8d-22-23     dynamique
  10.33.1.41            9c-fc-e8-36-7b-ee     dynamique
  10.33.1.243           34-7d-f6-5a-20-da     dynamique
  10.33.2.88            68-3e-26-7c-c3-1a     dynamique
```

### C. nmap
*(Toute cette partie ce fera depuis le réseau de chez moi, suite à quelques problèmes rencontrés)*

Après avoir réinitialiser ma conf réseau, je fais un scan de ping

![](https://i.imgur.com/4iujtcB.png)

puis avec `arp -a`

```
Interface : 192.168.1.70 --- 0x13
  Adresse Internet      Adresse physique      Type
  192.168.1.1           XX-XX-XX-XX-X3-a8     dynamique
  192.168.1.255         ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

### D. Modification d'adresse IP (part 2)
*(Toute cette partie ce fera depuis le réseau de chez moi)*

Au vu de mes résultats je conclu que ma box n'aime pas trop le fait de changer manuellement d'ip.
Car je n'arrive pas à avoir de la connection à internet après changement d'ip.

Voici tout de même mes résultats:

- le changement d'ip suivants les ip libre

![](https://i.imgur.com/GEr7YtA.png)

- Puis un scan de ping en utilisant `nmap`

![](https://i.imgur.com/pSHXUnf.png)

- Enfin le ping vers `1.1.1.1`, avec `ping 1.1.1.1`

```
PS C:\Users\LDLC> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 192.168.1.78 : Impossible de joindre l’hôte de destination.

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 1, reçus = 1, perdus = 0 (perte 0%),
```

## II. Exploration locale en duo

### 3. Modification d'adresse IP

On vérifie que les changements ont pris effet
Commande: ***ipconfig***

```
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::88d8:25ec:2492:3bbf%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
```

Puis on ping la deuxième machine: ***ping 192.168.10.1***

```
PS C:\Users\LDLC> ping 192.168.10.1

Envoi d’une requête 'Ping'  192.168.10.1 avec 32 octets de données :
Réponse de 192.168.10.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=64
Réponse de 192.168.10.1 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 192.168.10.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

On affiche la table ARP qui correspond a notre réseau Ethernet
Commande: ***arp -a***

```
Interface : 192.168.10.2 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.10.1          04-d4-c4-dd-3a-03     dynamique
  192.168.10.3          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

### 4. Utilisation d'un des deux comme gateway

Après avoir mis en place le réseau on fait un `ipconfig`
ainsi on peut voir que la carte Wi-Fi est déconnecté.

```
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::88d8:25ec:2492:3bbf%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.10.1

Carte réseau sans fil Wi-Fi :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . : auvence.co
```

Pour tester la connectivité on ping un DNS connu: ***ping 8.8.8.8***

```
PS C:\Users\LDLC> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 20ms, Moyenne = 20ms
```

On utilise ***tracert 8.8.8.8*** pour vérifier la gateway utilisé

```
PS C:\Users\LDLC> tracert 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1     1 ms     2 ms     1 ms  BASTIEN-PORTABLE [192.168.10.1]

Itinéraire déterminé.
```

### 5. Petit chat privé

Je ne disposais que d'un seul pc, donc suivant ce que tu nous avez conseillé je me suis connecter a moi-même avec netcat (un petit chat privé entre deux shell).

la connection s'est fait avec les commandes: `nc.exe -l -p 8888` pour le "shell serveur"
et `nc.exe 192.168.1.70 8888` pour le "shell client"

![](https://i.imgur.com/aEktgGs.png)

## III. Manipulations d'autres outils/protocoles côté client
*(Toute cette partie ce fera depuis le réseau de chez moi)*

### 1. DHCP

Grâce à la commande `ipconfig /all` on peut déterminer ip du serveur DHCP, ainsi que la date d'expiration du bail (un bail dure en général 24h dans une config réseau standard).
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
   Adresse physique . . . . . . . . . . . : 08-71-90-87-B9-8C
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::7967:acde:dfb4:2037%19(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.70(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 10:24:18
>> Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 10:24:18
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
>> Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 151548304
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-DE-BE-1A-08-71-90-87-B9-8C
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

### 2. DNS

Je peux obtenir l'ip du serveur DNS avec la commande `ipconfig /all`

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
   Adresse physique . . . . . . . . . . . : 08-71-90-87-B9-8C
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::7967:acde:dfb4:2037%19(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.70(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Bail obtenu. . . . . . . . . . . . . . : dimanche 19 septembre 2021 10:24:18
   Bail expirant. . . . . . . . . . . . . : lundi 20 septembre 2021 10:24:18
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
   Serveur DHCP . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . : 151548304
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-27-DE-BE-1A-08-71-90-87-B9-8C
>> Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.1
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```

Commande: `nslookup google.com`
```
PS C:\Users\LDLC> nslookup google.com
Serveur :   box
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80d::200e
          172.217.19.238
```

Commande: `nslookup ynov.com`
```
PS C:\Users\LDLC> nslookup ynov.com
Serveur :   box
Address:  192.168.1.1

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

La commande `nslookup` affiche en premier lieu le nom et addresse ip du serveur DNS que ma machine utilise,
Puis le nom et l'adresse ip *(que la commande détermine)* du serveur DNS que le domaine ciblé utilise si celui-ci n'est pas privé.

Commande: `nslookup 78.74.21.21`
```
PS C:\Users\LDLC> nslookup 78.74.21.21
Serveur :   box
Address:  192.168.1.1

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```

Commande: `nslookup 92.146.54.88`
```
PS C:\Users\LDLC> nslookup 92.146.54.88
Serveur :   box
Address:  192.168.1.1

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```

On utilise le `nslookup` pour faire un *reverse lookup*, la commande renvoie le nom et addresse ip du serveur DNS que ma machine utilise.
Puis trouve le nom (la donnée que la commande détermine) et affiche l'ip du serveur DNS ciblé en paramètre de commande.


### 6. Firewall - IV. Wireshark

**Je n'ai pas pu faire ces parties car je n'ai pas deux PC chez moi**