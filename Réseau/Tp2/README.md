# TP2 : On va router des trucs

## I. ARP

### 1. Echange ARP

commande: `ping 10.2.1.12`
```
[kai@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.836 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.530 ms

--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1030ms
rtt min/avg/max/mdev = 0.530/0.683/0.836/0.153 ms
```

commande: `ping 10.2.1.11`
```
[kai@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=0.216 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.399 ms

--- 10.2.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1029ms
rtt min/avg/max/mdev = 0.216/0.307/0.399/0.093 ms
```

J'affiche la table ARP des deux machines avec la commande `ip neigh show`

node1
```
[kai@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:42 DELAY
10.2.1.12 dev enp0s8 lladdr 08:00:27:c2:a5:4c STALE
```

node2
```
[kai@node2 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:42 REACHABLE
10.2.1.11 dev enp0s8 lladdr 08:00:27:6e:40:30 STALE
```

Donc la MAC de node1 est `08:00:27:6e:40:30`,
et celle de node2 est `08:00:27:c2:a5:4c``

Je prouve que l'info est correct avec la table ARP de node1 `ip neigh show`
```
  [kai@node1 ~]$ ip neigh show
  10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:42 DELAY
>>10.2.1.12 dev enp0s8 lladdr 08:00:27:c2:a5:4c STALE
```

Ainsi que la MAC de node2 `ip a`
```
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
>>  link/ether 08:00:27:c2:a5:4c brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fec2:a54c/64 scope link
       valid_lft forever preferred_lft forever
```

### 2. Analyse de trames

J'effectue une capture de Tram avec la commande `sudo tcpdump -i enp0s8 -w mon_fichier.pcap`

| ordre | type trame  | source                      | destination                     |
|-------|-------------|-----------------------------|---------------------------------|
| 1     | Requête ARP | `node1` `08:00:27:c2:a5:4c` | `Broadcast` `FF:FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `08:00:27:6e:40:30` | `node1` `08:00:27:c2:a5:4c`     |
| 3     | Requête ARP | `node2` `08:00:27:6e:40:30` | `node1` `08:00:27:c2:a5:4c`     |
| 4     | Réponse ARP | `node1` `08:00:27:c2:a5:4c` | `node2` `08:00:27:6e:40:30`     |

Capture [tp2_arp.pcap](https://gitlab.com/CrZr/b2-2021/-/blob/main/R%C3%A9seau/Tp2/Capture/tp2_arp.pcap)

## II. Routage

### 1. Mise en place du routage

- Activer le routage sur le noeud `router.net2.tp2`

Commande: `sudo firewall-cmd --add-masquerade --zone=public --permanent`
```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- Ajouter les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se `ping`

Commande pour ajouter une route: `ip route add <ip réseau>/24 via <gateway> dev enp0s8`

*node1*
```
[kai@node1 ~]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
10.2.2.0/24 via 10.2.1.11 dev enp0s8
```

*marcel*
```
[kai@marcel ~]$ ip r s
10.2.1.0/24 via 10.2.2.11 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

*routeur*
```
[kai@router ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 101
10.2.2.0/24 dev enp0s9 proto kernel scope link src 10.2.2.11 metric 102
```

Commande: `ping 10.2.2.12` via node1

```
[kai@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.09 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.776 ms

--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.776/0.930/1.085/0.157 ms
```

### 2. Analyse de trames

- Analyse des échanges ARP

table ARP: `arp -a`

*node1*
```
[kai@node1 ~]$ arp -a
? (10.2.1.1) at 0a:00:27:00:00:42 [ether] on enp0s8
? (10.2.1.11) at 08:00:27:54:2a:c3 [ether] on enp0s8
```

*marcel*
```
[kai@marcel ~]$ arp -a
? (10.2.2.11) at 08:00:27:b7:3c:31 [ether] on enp0s8
? (10.2.2.1) at 0a:00:27:00:00:46 [ether] on enp0s8
```

*router*
```
[kai@router ~]$ arp -a
? (10.2.1.12) at 08:00:27:6e:40:30 [ether] on enp0s8
_gateway (10.0.2.2) at 52:54:00:12:35:02 [ether] on enp0s3
? (10.2.2.12) at 08:00:27:c2:a5:4c [ether] on enp0s9
? (10.2.1.1) at 0a:00:27:00:00:42 [ether] on enp0s8
```

J'en déduit que chaque réseau échange des protocoles ARP uniquement avec le `router`

En utilisant les capture réseau j'ai pu établir le tableau ci-dessous.
| ordre | type trame  | IP source | MAC source                    | IP destination | MAC destination              |
|-------|-------------|-----------|-------------------------------|----------------|------------------------------|
| 1     | Requête ARP | x         | `node1` `08:00:27:6e:40:30`   | x              | `Broadcast` `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `router` `08:00:27:54:2a:c3`  | x              | `node1` `08:00:27:6e:40:30`  |
| 3     | Requête ARP | x         | `router` `08:00:27:b7:3c:31`  | x              | `Broadcast` `FF:FF:FF:FF:FF` |
| 4     | Réponse ARP | x         | `marcel` `08:00:27:c2:a5:4c`  | x              | `router` `08:00:27:b7:3c:31` |
| 5     | Ping        | 10.2.1.12 | `node1` `08:00:27:6e:40:30`   | 10.2.2.12      | `marcel` `08:00:27:c2:a5:4c` |
| 6     | Pong        | 10.2.2.12 | `marcel` `08:00:27:c2:a5:4c`  | 10.2.1.12      | `node1` `08:00:27:6e:40:30`  |

captures 
- [tp2_routage_node1.pcap](https://gitlab.com/CrZr/b2-2021/-/blob/main/R%C3%A9seau/Tp2/Capture/tp2_routage_node1.pcap)
- [tp2_routage_marcel.pcap](https://gitlab.com/CrZr/b2-2021/-/blob/main/R%C3%A9seau/Tp2/Capture/tp2_routage_marcel.pcap)
- [tp2_routage_router.pcap](https://gitlab.com/CrZr/b2-2021/-/blob/main/R%C3%A9seau/Tp2/Capture/tp2_routage_router.pcap)

### 3. Accès internet

***node1***

Commande: `ip r s`

```
[kai@node1 ~]$ ip r s
default via 10.2.1.11 dev enp0s8 proto static metric 100
10.0.2.0/24 via 10.2.1.11 dev enp0s8
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
10.2.2.0/24 via 10.2.1.11 dev enp0s8 proto static metric 100
```

Commande: `ping 8.8.8.8`

```
[kai@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=40.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=41.4 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 40.895/41.148/41.402/0.324 ms
```

Commande: `dig ynov.com`

```
[kai@node1 ~]$ dig ynov.com

;; ANSWER SECTION:
ynov.com.               6243    IN      A       92.243.16.143

;; Query time: 40 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 17:00:42 CEST 2021
;; MSG SIZE  rcvd: 53
```

***marcel***

Commande: `ip r s`

```
[kai@marcel ~]$ ip r s
default via 10.2.2.11 dev enp0s8 proto static metric 100
10.0.2.0/24 via 10.2.2.11 dev enp0s8
10.2.1.0/24 via 10.2.2.11 dev enp0s8 proto static metric 100
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```

Commande: `ping 8.8.8.8`

```
[kai@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=39.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=37.10 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 37.989/38.685/39.382/0.723 ms
```

Commande: `dig github.com`

```
[kai@marcel ~]$ dig github.com

;; ANSWER SECTION:
github.com.             59      IN      A       140.82.121.4

;; Query time: 39 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 17:08:36 CEST 2021
;; MSG SIZE  rcvd: 55
```
- Analyse de Tram:

| ordre | type trame | IP source           | MAC source                   | IP destination      | MAC destination              |
|-------|------------|---------------------|------------------------------|---------------------|------------------------------|
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:6e:40:30`  | `8.8.8.8`           | `router` `08:00:27:54:2a:c3` |
| 2     | pong       | `8.8.8.8`           | `router` `08:00:27:54:2a:c3` | `node1` `10.2.1.12` | `node1` `08:00:27:6e:40:30`  |

Capture: [tp2_routage_internet.pcap](https://gitlab.com/CrZr/b2-2021/-/blob/main/R%C3%A9seau/Tp2/Capture/tp2_routage_internet.pcap)

## III. DHCP

### 1. Mise en place du serveur DHCP

Configuration du dhcp
```
default-lease-time 600;
max-lease-time 7200;
authoritative;

subnet 10.2.1.0 netmask 255.255.255.0 {
      range dynamic-bootp 10.2.1.13 10.2.1.254;
      option broadcast-address 10.2.1.255;
      option routers 10.2.1.11;
}
```

Lancement du dhcp
```
[kai@node1 ~]$ sudo systemctl enable --now dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
[kai@node1 ~]$ sudo firewall-cmd --add-service=dhcp
success
[kai@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
```