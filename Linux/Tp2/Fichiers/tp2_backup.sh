#!/bin/bash
# Simple Script Backup
# kai 24/10/21

now =$ (date +%y%m%d_%H%M%S);
backup_name = tp2_backup_$now.tar.gz;

source_file =$ (readlink -f $2);
destination =$ (readlink -f $1);
archive =$ (readlink -f $backup_name);

tar -czvf $backup_name $source_file;
rsync -av --remove-source-files  $archive $destination;

# Delete more than 5 backup
cd $destination;
ls -tQ  | tail -n+6 | xargs rm &>/dev/null;
cd -;