# TP2 pt. 2 : Maintien en condition opérationnelle

| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|----------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 19999/tcp   | ?              |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | 19999/tcp   | ?              |

## I. Monitoring

### 2. Setup

**Setup Netdata**


**Sur `web`**
```
[kai@web ~]$ sudo su -
[...]
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[...]
[root@web ~]# exit
```

**Manipulation du service netdata**

**Sur `web`**
```
[kai@web ~]$ sudo systemctl status netdata
[sudo] password for kai:
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-24 09:41:54 CEST; 24min ago
```

**Le service utilise le port 19999**
```
[kai@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[kai@web ~]$ sudo firewall-cmd --reload
success
```

**ajouter sur discord**
```
[kai@web ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[...]
#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897423401260560394/24b_weARowRb5l9j98TdpiyhC6fOh92lrnx6c6WIh_PlTMIuVfhdQe5jN9ZP7CHfwUBV"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"

[...]
```

**Vérifier le bon fonctionnement**
```
[kai@web ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
2021-10-24 10:27:05: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is WARNING to 'alarms'
# OK

# SENDING TEST CRITICAL ALARM TO ROLE: sysadmin
2021-10-24 10:27:06: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CRITICAL to 'alarms'
# OK

# SENDING TEST CLEAR ALARM TO ROLE: sysadmin
2021-10-24 10:27:06: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
```
```
[kai@web ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```

**Faire les même commandes pour `db.tp2.linux`**
> En changeant le Webhook pour plus de visibilité
```
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/902113880329826344/BwgNZi-9gO9eiB7cpb8YGr_LQxlO1h-mCEUeB6cSUE5evkUiGaO4QG60f4n4pmMDF7fL"
```

**Config alerting**

- créez une nouvelle alerte pour recevoir une alerte à 50% de remplissage de la RAM
```
[kai@web ~]$ sudo vim /opt/netdata/etc/netdata/conf.d/health.d/ram.conf

[...]
    alarm: ram_in_use
       on: system.ram
    class: Utilization
     type: System
component: Memory
       os: linux
    hosts: *
     calc: ($used - $used_ram_to_ignore) * 100 / ($used + $cached + $free + $buffers)
    units: %
    every: 10s
     warn: $this > (($status >= $WARNING)  ? (50) : (90))
     crit: $this > (($status == $CRITICAL) ? (90) : (98))
[...]

```

- testez que votre alerte fonctionne
```
[kai@web ~]$ stress --vm 4 -t 10s
```
![](https://i.imgur.com/VSgaRQa.png)

# II. Backup

| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 19999/tcp   | ?             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 19999/tcp   | ?             |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 19999/tcp   | ?             |

🖥️ **VM `backup.tp2.linux`**

## 2. Partage NFS

🌞 **Setup environnement**

- créer un dossier `/srv/backups/`
- il contiendra un sous-dossier pour chaque machine du parc
- il existera un partage NFS pour chaque machine (principe du moindre privilège)

🌞 **Setup partage NFS**
```
[kai@backup ~]$ mkdir /srv/backup
[kai@backup ~]$ sudo chown -R kai:kai /srv/backup/
```
```
[kai@backup ~]$ cat /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = tp2.linux

[...]
```
```
[kai@backup ~]$ sudo mkdir -p /srv/backup/{web,db}.tp2.linux
[kai@backup ~]$ ls /srv/backup
db.tp2.linux  web.tp2.linux
```
```
[kai@backup ~]$ cat /etc/exports
/srv/backup/web.tp2.linux web(rw, no_root_squash) /srv/backup/db.tp2.linux db(rw, no_root_squash)
```
```
[kai@backup ~]$ sudo systemctl enable --now nfs-server
```
```
[kai@backup ~]$ sudo firewall-cmd --add-service={nfs3,mountd,rpc-bind} --permanent
success
[kai@backup ~]$ sudo firewall-cmd --reload
success
```

🌞 **Setup points de montage sur `web.tp2.linux`**

- monter le dossier `/srv/backups/web.tp2.linux` du serveur NFS dans le dossier `/srv/backup/` du serveur Web
```
[kai@web ~]$ sudo mkdir /srv/backup
```

- vérifier...
  - avec une commande `mount` que la partition est bien montée
  ```
  [kai@web ~]$ sudo mount /srv/backup/ -v
  mount.nfs: timeout set for Mon Oct 25 18:15:06 2021
  mount.nfs: trying text-based options 'vers=4.2,addr=10.102.1.13,clientaddr=10.102.1.11'
  ```

  - avec une commande `df -h` qu'il reste de la place
  ```
  [kai@web ~]$ df -h
  [...]
  backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  2.1G  4.1G  34% /srv/backup
  ```
  
  - avec une commande `touch` que vous avez le droit d'écrire dans cette partition
  ```
  [kai@web ~]$ touch /srv/backup
  [kai@web ~]$
  ```
- faites en sorte que cette partition se monte automatiquement grâce au fichier `/etc/fstab`
```
backup:/srv/backup/web.tp2.linux /srv/backup/ nfs    defaults        0 0
```

## 3. Backup de fichiers

🌞 **Rédiger le script de backup `/srv/tp2_backup.sh`**

- le script crée une archive compressée `.tar.gz` du dossier ciblé
  - cela se fait avec la commande `tar`
- l'archive générée doit s'appeler `tp2_backup_YYMMDD_HHMMSS.tar.gz`
  - vous remplacerez évidemment `YY` par l'année (`21`), `MM` par le mois (`10`), etc.
  - ces infos sont déterminées dynamiquement au moment où le script s'exécute à l'aide de la commande `date`
- le script utilise la commande `rsync` afin d'envoyer la sauvegarde dans le dossier de destination
- il **DOIT** pouvoir être appelé de la sorte :

```bash
$ ./tp2_backup.sh <DESTINATION> <DOSSIER_A_BACKUP>
```

📁 [**tp2_backup.sh**](https://gitlab.com/CrZr/b2-2021/-/blob/main/Linux/Tp2/Fichiers/tp2_backup.sh)

🌞 **Tester le bon fonctionnement**

- exécuter le script sur le dossier de votre choix
```
[kai@web ~]$ ./tp2_backup.sh /tmp/kaikai /home/kai/toto
tp2_backup_211024_184841.tar.gz

sent 397 bytes  received 43 bytes  880.00 bytes/sec
total size is 270  speedup is 0.61
```

- prouvez que la backup s'est bien exécutée
```
[kai@web ~]$ ls -al /tmp/kaikai
total 24
drwxrwxr-x.  2 kai  kai   201 Oct 24 18:48 .
drwxrwxrwt. 11 root root 4096 Oct 24 18:46 ..
-rw-rw-r--.  1 kai  kai   270 Oct 24 18:48 tp2_backup_211024_184827.tar.gz
```

---

### A. Unité de service

🌞 **Créer une *unité de service*** pour notre backup

- le contenu :

```bash
[kai@web ~]$ cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/tp2_backup.sh /srv/backup/ /home/kai/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

🌞 **Tester le bon fonctionnement**

- prouvez que la backup s'est bien exécutée
```
[kai@web ~]$ ls -al /srv/backup
total 8
drwxrwxrwx. 2 kai  kai    45 Oct 24 19:01 .
drwxr-xr-x. 3 root root   20 Oct 24 16:23 ..
-rw-r--r--. 1 root root 4165 Oct 24 19:01 tp2_backup_211024_190144.tar.gz
```

---

### B. Timer

Un *timer systemd* permet l'exécution d'un *service* à intervalles réguliers.

🌞 **Créer le *timer* associé à notre `tp2_backup.service`**

- contenu du fichier :

```bash
[kai@web ~]$ cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

🌞 **Activez le timer**

- démarrer le *timer*
```
[kai@web ~]$ sudo systemctl enable --now tp2_backup.timer
[sudo] password for kai:
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
```

- prouver que...
```
[kai@web ~]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Mon 2021-10-24 19:09:57 CEST; 10s ago
  Trigger: Mon 2021-10-24 19:11:00 CEST; 52s left
```

🌞 **Tests !**

- vérifiez que la backup s'exécute correctement
```
[kai@web ~]$ ls -al /srv/backup
total 40
drwxrwxrwx. 2 kai  kai   201 Oct 24 19:12 .
drwxr-xr-x. 3 root root   20 Oct 24 16:23 ..
-rw-r--r--. 1 root root 4165 Oct 24 19:11 tp2_backup_211024_191104.tar.gz
-rw-r--r--. 1 root root 4165 Oct 24 19:12 tp2_backup_211024_191204.tar.gz
```

---

### C. Contexte

🌞 **Faites en sorte que...**

- la destination est le dossier NFS monté depuis le serveur `backup.tp2.linux`
```
ExecStart=/home/kai/tp2_backup.sh /srv/backup/ /var/www/sub-domains/linux.tp2.web/html/
```

- la sauvegarde s'exécute tous les jours à 03h15 du matin
```
OnCalendar=*-*-* 03:15:00
```

- prouvez avec la commande `sudo systemctl list-timers` que votre *service* va bien s'exécuter la prochaine fois qu'il sera 03h15
```
[kai@web ~]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED       UNIT                         ACT>
Tue 2021-10-25 03:15:00 CEST  7h left       Mon 2021-10-24 19:35:05 CEST  2min 28s ago tp2_backup.timer
```

📁 [**tp2_backup.timer**](https://gitlab.com/CrZr/b2-2021/-/blob/main/Linux/Tp2/Fichiers/tp2_backup.timer)
📁 [**tp2_backup.service**](https://gitlab.com/CrZr/b2-2021/-/blob/main/Linux/Tp2/Fichiers/tp2_backup.service)

## 5. Backup de base de données

Sauvegarder des dossiers c'est bien. Mais sauvegarder aussi les bases de données c'est mieux.

🌞 **Création d'un script `/srv/tp2_backup_db.sh`**

- il utilise la commande `mysqldump` pour récupérer les données de la base de données
- cela génère un fichier `.sql` qui doit ensuite être compressé en `.tar.gz`
- il s'exécute sur la machine `db.tp2.linux`
- il s'utilise de la façon suivante :

```bash
$ ./tp2_backup_db.sh <DESTINATION> <DATABASE>
```

📁 [**tp2_backup_db.sh**](https://gitlab.com/CrZr/b2-2021/-/blob/main/Linux/Tp2/Fichiers/tp2_backup_db.sh)  

🌞 ***Unité de service***

- pareil que pour la sauvegarde des fichiers ! On va faire de ce script une *unité de service*.
- votre script `/srv/tp2_backup_db.sh` doit pouvoir se lancer grâce à un *service* `tp2_backup_db.service`
- le *service* est exécuté tous les jours à 03h30 grâce au *timer* `tp2_backup_db.timer`
- prouvez le bon fonctionnement du *service* ET du *timer*

📁 [**tp2_backup_db.timer**](https://gitlab.com/CrZr/b2-2021/-/blob/main/Linux/Tp2/Fichiers/tp2_backup_db.timer)  
📁 [**tp2_backup_db.service**](https://gitlab.com/CrZr/b2-2021/-/blob/main/Linux/Tp2/Fichiers/tp2_backup_db.service)
