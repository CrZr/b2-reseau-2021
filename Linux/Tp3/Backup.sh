#!/bin/bash
# Simple Script Backup
# kai 16/11/21

now =$ (date +%y%m%d_%H%M%S);
backup_name = tp3_backup_$now.tar.gz;

source_file =$ ('/var/www/html/moodle/backup/');
destination =$ ('/srv/backup/');
archive =$ (readlink -f $backup_name);

tar -czvf $backup_name $source_file;
rsync -av --remove-source-files  $archive $destination;

# Delete more than 5 backup
cd $destination;
ls -tQ  | tail -n+6 | xargs rm &>/dev/null;
cd -;