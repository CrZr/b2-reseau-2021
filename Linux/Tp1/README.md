# TP1 : (re)Familiaration avec un système GNU/Linux
## 0. Préparation de la machine

### Setup de deux machines Rocky Linux configurée de façon basique.

- **Un accès internet**

Carte réseau: ***ip a***

```
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0a:71:fb brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86322sec preferred_lft 86322sec
    inet6 fe80::a00:27ff:fe0a:71fb/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Route par défaut: ***ip r s***

*node1*
```
[kai@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
```

*node2*
```
[kai@node2 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
```

ping internet: ***ping 8.8.8.8*** 

*node1*
```
[kai@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=18.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.8 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.068/18.409/18.751/0.367 ms
```

*node2*
```
[kai@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=40.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=18.4 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 18.439/29.597/40.756/11.159 ms
```

- **Un accès a un réseau local**

Activation de la carte réseau host-only: ***nmcli con up enp0s8***

On définit les ip des deux machines en statique en modifiant: `/etc/sysconfig/network-scripts/ifcfg-enp0s8`

On ping les deux machines entre elles: ***ping {ip address}***

*node1*
```
[kai@node1 ~]$ ping 10.101.1.12
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.944 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.413 ms

--- 10.101.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.413/0.678/0.944/0.266 ms
```

*node2*
```
[kai@node2 ~]$ ping 10.101.1.11
PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.372 ms
64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.400 ms

--- 10.101.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1017ms
rtt min/avg/max/mdev = 0.372/0.386/0.400/0.014 ms
```

- **Changement de nom pour les machines**

On modifie le nom en éditant: `/etc/hostname`

puis on affiche avec ***hostnamectl***

*node1*
```
[kai@node1 ~]$ hostnamectl
   Static hostname: node1.tp1.b2
```

*node2*
```
[kai@node2 ~]$ hostnamectl
   Static hostname: node2.tp1.b2
```

- **DNS = 1.1.1.1**

Commande: `dig ynov.com`

*ip qui correspond au nom demandé*
```
;; ANSWER SECTION:
ynov.com.               6602    IN      A       92.243.16.143
```

*ip du serveur qui a répondu*
```
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

- **Les machines peuvent se joindre par leurs noms respectifs**

*node1*
```
[kai@node1 ~]$ ping node2
PING node2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.389 ms
64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.393 ms
^C
--- node2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1006ms
rtt min/avg/max/mdev = 0.389/0.391/0.393/0.002 ms
```

*node2*
```
[kai@node2 ~]$ ping node1
PING node1 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.441 ms
64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.424 ms
^C
--- node1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 0.424/0.432/0.441/0.022 ms
```

- **Firewall**

Commande: `firewall-cmd --list-all`

*node1*
```
[kai@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

*node2*
```
[kai@node2 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 22/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

## I. Utilisateurs

### 1. Création et configuration

- **Nouvel utilisateur**
Commande: `useradd <nom_nouvel_utilisateur> -m -s /bin/sh`

```
[kai@node1 ~]$ sudo useradd toto -m -s /bin/sh
[sudo] password for kai:
[kai@node1 ~]$ ls /home/
kai  toto
```

- **Nouveau groupe**

Commande: `groupadd admins`

```
[kai@node1 ~]$ sudo !!
sudo groupadd admins
[kai@node1 ~]$
```

- **Admins ajouté à /etc/sudoers**

Commande: `sudo nano /etc/sudoers`

```
%admins ALL=(ALL) ALL
```

- **Ajoute de Toto dans le groupe admins**

Commande: `usermod -aG admins toto`

```
[kai@node1 ~]$ groups toto
toto : toto admins
```

### 2. SSH

On crée la clé SSH:
```
PS C:\Users\LDLC> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\LDLC/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\LDLC/.ssh/id_rsa.
Your public key has been saved in C:\Users\LDLC/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:qHVi9BUymxQK3KUbTekZNXbrP1X3MMJHNvjZtLq+LU4 ldlc@MSIManoa
The key's randomart image is:
+---[RSA 4096]----+
|   ... .B+= ..+  |
|    ...*o* =.+ ..|
|      =.+o. +.+++|
|     . =o. . oo+=|
|      * S   . . o|
|     + o     o . |
|    .        E+  |
|            .o.. |
|            o+o. |
+----[SHA256]-----+
```

Je donne ma clé publique à la VM par SSH:
```
PS C:\Users\LDLC> ssh-copy-id -i ~/.ssh/id_rsa.pub kai@10.101.1.11
```

résultat:
```
PS C:\Users\LDLC> ssh kai@10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Sun Sep 26 16:25:30 2021 from 10.101.1.1
[kai@node1 ~]$
```

## II. Partitionnement

### 1. Préparation de la VM

Je regarde que mes deux disques sont bien la
```
[kai@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```
### 2. Partitionnement

```
[kai@node1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[kai@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
```
```
[kai@node1 ~]$ sudo vgcreate volume /dev/sdb
  Volume group "volume" successfully created
[kai@node1 ~]$ sudo vgextend volume /dev/sdc
  Volume group "volume" successfully extended
```
```
[kai@node1 ~]$ sudo lvcreate -L 1G volume -n part1
  Logical volume "part1" created.
[kai@node1 ~]$ sudo lvcreate -L 1G volume -n part2
  Logical volume "part2" created.
[kai@node1 ~]$ sudo lvcreate -L 1G volume -n part3
  Logical volume "part3" created.
```
```
[kai@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 034860e3-90e9-470e-9ae1-ebe123bf3a5d
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[kai@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part2
...

[kai@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part3
...
```
```
[kai@node1 mnt]$ sudo mkdir part1
[kai@node1 mnt]$ sudo mkdir part2
[kai@node1 mnt]$ sudo mkdir part3
[kai@node1 mnt]$ sudo mount /dev/volume/part1 /mnt/part1
[kai@node1 mnt]$ sudo mount /dev/volume/part2 /mnt/part2
[kai@node1 mnt]$ sudo mount /dev/volume/part3 /mnt/part3
```
```
[kai@node1 mnt]$ mount
...
/dev/mapper/volume-part1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/volume-part2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/volume-part3 on /mnt/part3 type ext4 (rw,relatime,seclabel)
```
Dans le fichier `/etc/fstab`
```
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=3047bbca-1999-4eae-bda7-0b7a05fb0087 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0

/dev/volume/part1 /mnt/part1 ext4 defaults 0 0
/dev/volume/part2 /mnt/part2 ext4 defaults 0 0
/dev/volume/part3 /mnt/part3 ext4 defaults 0 0
```

Afin de vérifier le montage au redémarrage, j'unmount les part puis les remonte avec `sudo mount -av`
```
[kai@node1 mnt]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
...
/mnt/part1               : successfully mounted
...
/mnt/part2               : successfully mounted
...
/mnt/part3               : successfully mounted
```